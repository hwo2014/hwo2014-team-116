function SpeedAnalyzer(pieces, lanes, cornerAnalyzer) {
    var pieces = pieces;
    var cornerAnalyzer = cornerAnalyzer;
    var lanes = lanes;
    var speedAskedForCorner = -1;
    var nextCornerSpeed;
    var currentCornerSpeed = 0;
    var previousCarAngle = 0;

    this.getOptimalSpeed = function(currentSpeedPerTick, currentPosition, carPosition, currentThrottle) {

        var piece = pieces[currentPosition.pieceIndex];
        var nextCornerIndex = getNextCornerIndex(currentPosition.pieceIndex);

        if (speedAskedForCorner !== nextCornerIndex) {
            currentCornerSpeed = nextCornerSpeed;
            nextCornerSpeed = cornerAnalyzer.getCornerSpeed(nextCornerIndex);
            speedAskedForCorner = nextCornerIndex;
        }
        
        if (currentPosition.lap +1 === totalLaps && pieces[currentPosition.pieceIndex].length && nextCornerIndex < currentPosition.pieceIndex) {
            return 1;
        }
        if (Math.abs(myPosition.angle) > 58.5) {
            return 0;
        }


        //console.log("NEXT CORNER: " + nextCornerIndex + ", TARGET SPEED: " + nextCornerSpeed);

        var distanceToCorner = calculateDistanceToCurve(currentPosition, nextCornerIndex);
        //console.log("distance to corner (" + nextCornerIndex + "): " + distanceToCorner + ", TARGET SPEED: " + nextCornerSpeed)

        var needToBrake = needToBrakeNow(distanceToCorner, currentSpeedPerTick, nextCornerSpeed, nextCornerIndex, lanes[currentPosition.lane.endLaneIndex]);
        //console.log("brake: " + needToBrake);

        var targetSpeed = nextCornerSpeed;

        if (needToBrake ) {
            var nextSpeed = currentSpeedPerTick - currentSpeedPerTick*brakeForce;
            //var afterThatSpeed = nextSpeed - nextSpeed*brakeForce;


            if (nextSpeed < targetSpeed*10 && currentSpeedPerTick > targetSpeed) {
                return getExactBrakeSpeed(currentSpeedPerTick, targetSpeed);

            }
            else if(currentSpeedPerTick < targetSpeed) {
                return targetSpeed/10;
            }
            else {
                return 0;
            }
        }
        else if (piece.angle){
            return cornerAnalyzer.getCurveSpeed(carPosition, piece, currentThrottle);
            /*if(currentSpeedPerTick > targetSpeed) {
                var speed = getExactBrakeSpeed(currentSpeedPerTick, targetSpeed);
                if (speed < 0) {
                    speed = 0;
                }
                return speed;
            }
            else {
                return currentCornerSpeed;
            }*/
        }
        else {
            var maxSpeedAfterThrottle = currentSpeedPerTick + ((10-currentSpeedPerTick)*throttleForce)/10;
            if (distanceToCorner >= maxSpeedAfterThrottle) {
                return 1;
            }
            else {
                return getExactThrottleSpeed(currentSpeedPerTick, targetSpeed);
            }
        }
        
    };
    
    function getNextCornerIndex(currentIndex) {
        return cornerAnalyzer.getNextCornerId(currentIndex);
    }
    
    function needToBrakeNow(distance, speedPerTick, targetSpeed, pieceIndex, lane) {
        var brakeDistance = 0;
        var origSpeedPerTick = speedPerTick;
        
        while (speedPerTick > targetSpeed*10) {
            brakeDistance += speedPerTick;
            speedPerTick -= speedPerTick*brakeForce;
        }
                //console.log("brakedist: " + brakeDistance + ", distance: " + distance);
        var multiplier = 1;
        if (turboOn) {
            //multiplier = turbo.turboFactor;
        }
        if (brakeDistance*multiplier > distance) { // +calculateCornerLength(pieceIndex, lane)/2) {
            return true;

        }
        return false;
    }
    
    function calculateDistanceToCurve(piecePosition, cornerIndex) {

        var currentPiece = pieces[piecePosition.pieceIndex];
        var lane = lanes[piecePosition.lane.startLaneIndex];
        
        var distanceToGo = 0;
        var pieceDistance = piecePosition.inPieceDistance;
        
        if (currentPiece.angle) {
            var cornerLength = calculateCornerLength(piecePosition.pieceIndex, lane);
            distanceToGo = cornerLength - pieceDistance;
            
        }
        else {
            distanceToGo = currentPiece.length - pieceDistance;
        }
        
        var currentIndex = piecePosition.pieceIndex;
        if (cornerIndex > piecePosition.pieceIndex+1) {
            
            for (var i = piecePosition.pieceIndex+1; i < cornerIndex; i++) {
                if (pieces[i].length) {
                    distanceToGo += pieces[i].length;
                }
                else {
                    distanceToGo += calculateCornerLength(i, lane);
                }
            }
            
        }
        else if (cornerIndex < piecePosition.pieceIndex) {
            for (var i = piecePosition.pieceIndex+1; i < pieces.length; i++) {
                if (pieces[i].length) {
                    distanceToGo += pieces[i].length;
                }
                else {
                    distanceToGo += calculateCornerLength(i, lane);
                }
            }
            
            for (var i = 0; i < cornerIndex; i++) {
                if (pieces[i].length) {
                    distanceToGo += pieces[i].length;
                }
                else {
                    distanceToGo += calculateCornerLength(i, lane);
                }
            }
        }
        
        return distanceToGo;
        
    }
    
    function calculateCornerLength(index, lane) {
        var radius = pieces[index].radius;
        var angle = pieces[index].angle;
        
        var roadLength;
        if (angle > 0) {
            roadLength = (2 * Math.PI * (radius - lane.distanceFromCenter)) * Math.abs(pieces[index].angle) / 360;
        }
        else {
            roadLength = (2 * Math.PI * (radius + lane.distanceFromCenter)) * Math.abs(pieces[index].angle) / 360;
        }
        return roadLength;
    }
    
    
}