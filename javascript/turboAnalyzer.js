function TurboAnalyzer(pieces) {
    var pieces = pieces;
    var turboStartPieceIndex = -1;

    /**
     {
       "turboDurationMilliseconds": 500.0,
       "turboDurationTicks": 30,
       "turboFactor": 3.0
     }
     */
    var turbo;

    this.turboAvailable = function (turboObject, currentIndex) {
        turbo = turboObject;
        var counter = 0;
        // If we are on a straight piece
        if (pieces[currentIndex].length) {
            for (var i = 0; i < pieces.length; i++) {
                if (currentIndex+1 < pieces.length) {
                    currentIndex++;
                } else {
                    currentIndex = 0;
                }

                if (pieces[currentIndex].length) {
                    counter++;

                    // And also three following pieces are straight
                    if (counter >= 3) {
                        turboStartPieceIndex = currentIndex - 3;
                        return;
                    }
                } else {
                    counter = 0;
                }
            }
        }
    }

    this.turboNeeded = function (currentIndex) {
        if (turbo && turboStartPieceIndex == currentIndex) {
            return true;
        } else {
            return false;
        }
    }
}