function getExactBrakeSpeed(currentSpeedPerTick, targetSpeed) {
    var needToBrakeSpeed = currentSpeedPerTick-targetSpeed*10
    var brakePercent = needToBrakeSpeed/currentSpeedPerTick;
    var brake = 1-(brakePercent/brakeForce);

    if ((currentSpeedPerTick/10)*brake < 0.0001)
        return 0
    else
        return (currentSpeedPerTick/10)*brake;
}

function getExactThrottleSpeed(prevSpeedPerTick, targetSpeed) {  
    var fromMaxSpeed = 10-prevSpeedPerTick;
    var moreSpeedAfterFullThrottle = fromMaxSpeed/10 * throttleForce;

    var moreSpeedNeeded = targetSpeed*10 - prevSpeedPerTick;
    var percentOfFullThrottle = moreSpeedNeeded/moreSpeedAfterFullThrottle;

    var addSpeed = fromMaxSpeed * percentOfFullThrottle;

    if ((prevSpeedPerTick + addSpeed)/10 >0.999)
        return 1;
    else
        return (prevSpeedPerTick + addSpeed)/10;

}