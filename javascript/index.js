var net = require("net");
var fs = require('fs');
var JSONStream = require('JSONStream');

// file is included here:
eval(fs.readFileSync('utils.js')+'');
eval(fs.readFileSync('cornerAnalyzer.js')+'');
eval(fs.readFileSync('speedAnalyzer.js')+'');
eval(fs.readFileSync('turboAnalyzer.js')+'');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var pieces = [];
var lanes = [];
var cars = [];
var pieceIndex = -1;
var previousSpeed = 1;
var prevPrevSpeed;
var prevSpeedPerTick = 0;
var prevPiecePosition;
var prevPiece;
var switchSent;
var crash = false;
var positions;
var myPosition;
var prevAngle;
var cornerSpeed = 0.3;
var brakeForce = 0.02;
var throttleForce = 0.2;
var slidingToCorner = false;
var track;
var lap = -1;
var totalLaps;
var cornerAnalyzer;
var speedAnalyzer;
var turboAnalyzer;
var carId;
var longestStraightIndex;
var turboAvailable = false;
var turboOn = false;
var turbo;
var throttleForceSet = false;
var brakeForceSet = false;
var maxSpeedSet = false;
var maxSpeed;
var throttleMagnifier;

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
    //return send({ msgType: "joinRace", data: { trackName: "germany", carCount: 2, botId: { name: botName, key: botKey } } });
    return send({ msgType: "join", data: { name: botName, key: botKey  } });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  
    if (data.msgType === 'carPositions') {
        if (!crash) {
            try {
                carPositions(data);
            }
            catch (e) {
                console.log(e);
                send({
                msgType: "ping",
                data: {}
            });
            }
        }
        else {
            send({
                msgType: "throttle",
                data: 1
            });
        }

    }
    else {
        if (data.msgType === 'join') {
            console.log('Joined');
        }
        else if (data.msgType === 'gameInit') {  
            pieces = data.data.race.track.pieces;
            lanes = data.data.race.track.lanes;
            cars = data.data.race.cars;
            totalLaps = data.data.race.raceSession.laps;
            track = data.data.race.track.id;
            var oldCornerData = '';
            cornerAnalyzer = new CornerAnalyzer(pieces, oldCornerData);
            speedAnalyzer = new SpeedAnalyzer(pieces, lanes, cornerAnalyzer);
            turboAnalyzer = new TurboAnalyzer(pieces);
            log(JSON.stringify(data), true);
            console.log(JSON.stringify(data));
            getLongestStraight();
                
        }
        else if (data.msgType === 'turboAvailable') {
            console.log(JSON.stringify(data));
            turboAvailable = true;
            turbo = data.data;
            //turboAnalyzer.turboAvailable(data.data, getMyCarData().piecePosition.pieceIndex);
        }
        else if (data.msgType === 'turboEnd') {
            if (data.data.color === carId.color) {
                turboAvailable = false;
                turboOn = false;
                console.log(JSON.stringify(data));
            }
            else {
                
            }
        }
        else if (data.msgType === 'turboStart') {
            if (data.data.color === carId.color) {
                turboOn = true;
                console.log(JSON.stringify(data));
            }
            else {
                
            }
        }
        else if (data.msgType === 'gameStart') {
            console.log('Race started');
        }
        else if (data.msgType === 'gameEnd') {
            log(JSON.stringify(cornerAnalyzer.corners));
            writeLog();
            writeCorners();
            console.log('Race ended');
        } else if (data.msgType === 'yourCar') {
            carId = data.data;
        } else {
            console.log("Speed: " + previousSpeed);
            console.log(JSON.stringify(data));

            log(JSON.stringify(data));

            if (data.msgType === 'crash') {
                if (data.data.color === carId.color) {
                    cornerAnalyzer.crashHappened(myPosition.piecePosition, pieces[myPosition.piecePosition.pieceIndex], myPosition.angle);
                    log(JSON.stringify(cornerAnalyzer.corners));
                    writeLog();
                    crash = true;
                }
                else {

                }
            }
            else if (data.msgType === 'spawn') {
                if (data.data.color === carId.color) {
                    crash = false;
                    if (previousSpeed === 0) {
                        previousSpeed = 1;
                    }
                }
            }
        }
        
        send({
             msgType: "ping",
             data: {}
         });
        
        
    }
});

jsonStream.on('error', function() {
    return console.log("disconnected");
});

function getLongestStraight() {
    var startPiece = 0;
    var currentPieces = 0;
    var maxPieces = 0;
    var maxPiece = 0;
    var goingStraight = false;
    for (var i = 0; i < pieces.length; i++) {
        if (!goingStraight && pieces[i].length) {
            goingStraight = true;
            currentPieces++;
            startPiece = i;
        }
        else if (goingStraight && pieces[i].length) {
            currentPieces++;
        }
        else {
            goingStraight = false;
            if (currentPieces > maxPieces) {
                maxPieces = currentPieces;
                maxPiece = startPiece;
            }
        }
    }
    if (goingStraight) {
        for (var i = 0; i < pieces.length; i++) {
            if (pieces[i].angle) {
                currentPieces += i;
            }
        }
    }
    if (currentPieces > maxPieces) {
        maxPieces = currentPieces;
        maxPiece = startPiece;
    }
    
    longestStraightIndex = maxPiece;
}

function carPositions(data) {
    positions = data.data;
    myPosition = getMyCarData();
    
    var piecePosition = myPosition.piecePosition;
    var index = piecePosition.pieceIndex;
    
    if (pieceIndex !== index) {
        if (pieceIndex === 0) {
        	lap = piecePosition.lap;
        	console.log("Lap: " + lap);
        }
        if (lap >= totalLaps) {
            send({msgType: "ping", data: {} });
            return;
        }
    
        console.log("piece: " + index + ", pieceAngle: " + pieces[index].angle+ ", radius: " + pieces[index].radius);
        log("piece: " + index + ", pieceAngle: " + pieces[index].angle, false);
        
        if (turboAvailable && longestStraightIndex === index && !turboOn) {
            send({
                msgType: "turbo",
                data: "WROOOOOM"
            });
            return;
        }
        
    }
    
    var speedPerTick = getSpeedPerTick(prevPiecePosition, piecePosition, data.gameTick);
    var speed = speedAnalyzer.getOptimalSpeed(speedPerTick, piecePosition, myPosition, speedPerTick/10);

     //       getThrottle(prevPiecePosition, piecePosition, speedPerTick, myPosition.angle);
    
    cornerAnalyzer.setPositionData(myPosition, speedPerTick, speed, lap);


    

    if (pieces[getNextPiece(index)].switch) {
        switchDirection(piecePosition);
    }

//speed= 0.6;
    prevPiecePosition = piecePosition;
    prevPrevSpeed = previousSpeed;    
    prevAngle = myPosition.angle;    
    pieceIndex = index;
    
    if (maxSpeedSet === false) {
        speed = 0.4;
    }
    else if (maxSpeedSet && speed !== 1) {
        speed = speed / throttleMagnifier;
    }

    if (speed > 1.0) {
        speed = 1.0;
    }
    else if (speed < 0) {
        speed = 0;
    }
    
    previousSpeed = speed;
    
    
      
    send({
        msgType: "throttle",
        data: speed,
        gameTick: data.gameTick
    });
}

function getMyCarData() {
    for (var i = 0; i < positions.length; i++) {
        if (positions[i].id.name === carId.name && positions[i].id.color === carId.color) {
            return positions[i];
        }
    }
}

function switchDirection(piecePosition) {

    var nextPieceIndex = getNextPiece(piecePosition.pieceIndex);

    var angle = 0;
    for (var i = nextPieceIndex+1; i < pieces.length; i++) {
        
        if (i > nextPieceIndex && pieces[i].switch) {
            break;
        }
        else if (pieces[i].angle) {
            angle += pieces[i].angle;
        }
    }
    
    if (switchSent !== nextPieceIndex) {
        if (angle > 0) {  
            send({
                msgType: "switchLane",
                data: "Right"
            });
        }
        else if (angle < 0) {
            send({
                msgType: "switchLane",
                data: "Left"
            });
        }       
        switchSent = nextPieceIndex;
    }
}

function getNextPiece(index) {
    if (index < (pieces.length-1)) {
        return index+1;
    }
    else {
        return 0;
    }
}

function getSpeedPerTick(prevPosition, curPosition, gameTick) {  
    var speedPerTick = 0;
    
    if (prevPosition) {
        var prevPiece = pieces[prevPosition.pieceIndex];
        if (!prevPiecePosition) {
            speedPerTick = curPosition.inPieceDistance;
        }
        else {
            if (prevPosition.pieceIndex !== curPosition.pieceIndex) {
                var speedChange = previousSpeed/throttleMagnifier - prevSpeedPerTick/10
                if (speedChange < 0.001) {
                    speedPerTick = getSpeedAfterBrake(prevSpeedPerTick, previousSpeed);
                }
                else if (speedChange > 0.001) {
                    speedPerTick = getSpeedAfterThrottle(prevSpeedPerTick, previousSpeed);
                }
                else {
                    speedPerTick = prevSpeedPerTick;
                }
            }
            else {
                speedPerTick = curPosition.inPieceDistance - prevPiecePosition.inPieceDistance;
            }
        }
        
        if (speedPerTick !== 0) {
            //if (speedPerTick/10 < cornerSpeed) {
                console.log("tick: " + gameTick + " - distanceSpeed: " +  speedPerTick + " (" + (speedPerTick-prevSpeedPerTick).toFixed(3) + " " + ((speedPerTick-prevSpeedPerTick)/prevSpeedPerTick*100).toFixed(3) + "), prevSpeed: " + previousSpeed + ", angle: " + myPosition.angle);
            //}
            log("distanceSpeed: " +  speedPerTick + " (" + (speedPerTick-prevSpeedPerTick) + " " + ((speedPerTick-prevSpeedPerTick)/prevSpeedPerTick*100).toFixed(3) + "), curSpeed: " + previousSpeed + ", angle: " + myPosition.angle, false);
            
            if (throttleForceSet === false && speedPerTick-prevSpeedPerTick > 0) {
                throttleForce = (speedPerTick-prevSpeedPerTick)*2.5;
                throttleForceSet = true;
                console.log("ThrottleForce: " + throttleForce);
            }
            else if (brakeForceSet === false && (speedPerTick-prevSpeedPerTick)/prevSpeedPerTick < 0 && previousSpeed === 0) {
                brakeForceSet = true;
                brakeForce = (speedPerTick-prevSpeedPerTick)/prevSpeedPerTick *-1;
                console.log("BrakeForce: " + brakeForce);
            }
            
            if (maxSpeedSet === false && Math.abs((speedPerTick-prevSpeedPerTick)) < 0.0001 && curPosition.pieceIndex === pieceIndex) {
                maxSpeedSet = true;
                maxSpeed = speedPerTick/4*10;
                throttleMagnifier = maxSpeed/10;
                console.log("MaxSpeed: " + maxSpeed + ", throttleMaginifier: " + maxSpeed/10);
            }
            
        }
    }
    prevSpeedPerTick = speedPerTick;
    return speedPerTick;
}

            
function getSpeedAfterBrake(tickSpeed, throttleSpeed) {
    var b = throttleSpeed / (tickSpeed/10);
    var bp = (1 - b) * brakeForce;
    var nbs = tickSpeed * bp;
    var target = tickSpeed - nbs;
    
    return target;
}

function getSpeedAfterThrottle(tickSpeed, throttleSpeed) {
                
    return tickSpeed + ((10-tickSpeed)*throttleForce)/10;

}

var logContainer = [];
function log(text, init) {
    logContainer.push(text);
}

function writeLog() {
    fs.writeFile('helloworld.txt', logContainer.join("\n"), function (err) {
            if (err) return console.log(err);
    }); 
    
}

function writeCorners() {
    fs.writeFile(track + '.txt', JSON.stringify(cornerAnalyzer.corners), function (err) {
            if (err) return console.log(err);
    }); 
}