function CornerAnalyzer(pieces, oldData) {
    var that = this;
    this.corners = [];
    var magicNumber = 0.45;
    
    init();
    
    function init () {
        if (oldData) {
            var corners = JSON.parse(oldData);
            for (var i = 0; i < corners.length; i++) {
                var restoreCorner = corners[i];
                var tempCorner = new corner(restoreCorner.startIndex);
                tempCorner.restore(restoreCorner);
                
                that.corners.push(tempCorner)
            }
        }
        else {
            
            
            var c = null;
            var prevAngle;
            var prevRadius;
            
            for (var i = 0; i < pieces.length; i++) {
                if (pieces[i].angle) {
                    if (c === null) {
                        c = new corner(i);
                        c.pieces.push(pieces[i]);
                        c.angle = prevAngle = pieces[i].angle;
                        c.radius = prevRadius = pieces[i].radius;
                    }
                    else {
                        if (pieces[i].angle === prevAngle && pieces[i].radius === prevRadius) {
                            c.pieces.push(pieces[i]);
                        }
                        else {
                            addCorner(c, i-1);
                            c = new corner(i);
                            c.pieces.push(pieces[i]);
                            c.angle = pieces[i].angle;
                            prevAngle = c.angle;
                            c.radius = pieces[i].radius;
                            prevRadius = c.radius;
                        }
                    }
                }
                else {
                    if (c !== null) {
                        addCorner(c, i-1);
                        c = null;
                    }
                    prevAngle = 0;
                    prevRadius = 0;
                }
            }
            
            if (c !== null) {
                addCorner(pieces.length-1);
            }
            
            function addCorner(cnr, endIndex) {
                cnr.endIndex = endIndex;
                var cornerToPush = cnr;
                cornerToPush.init();
                that.corners.push(cornerToPush);
            }
 
        }
    }

    this.driftAngleIncreaseAlertHappened = function(position, piece) {
        var indx = position.pieceIndex;
        if (!piece.angle) {
            indx--;
        }

        for(var i = 0; i < that.corners.length; i++) {
            if (indx >= that.corners[i].startIndex && indx <= that.corners[i].endIndex) {
                that.corners[i].driftAngleIncreaseAlert = true;
            }
        }
    }
    
    this.crashHappened = function(position, piece, carAngle) {
        var indx = position.pieceIndex;
        if (!piece.angle) {
            indx--;
        }
        
        for(var i = 0; i < that.corners.length; i++) {
            if (indx >= that.corners[i].startIndex && indx <= that.corners[i].endIndex) {
                that.corners[i].crashAngle = carAngle;
                if (that.corners[i].passed) {
                    that.corners[i].passed = false;
                }
                else {
                    that.corners[i].prevSpeed -= 0.01;
                }
            }
        }
    };
    
    this.setPositionData = function(position, speedPerTick, throttleSpeed, lap) {
    
        var indx = position.piecePosition.pieceIndex;
        
        for(var i = 0; i < that.corners.length; i++) {
            if (indx >= that.corners[i].startIndex  && indx <= that.corners[i].endIndex) {
        
                var angle = 0;
                if (position.angle) {
                    angle = position.angle;
                }
                
                
                that.corners[i].addToAngles(angle);
                if (that.corners[i].throttleSpeeds.length === 0) {
                    if (that.corners[i].speed === 0) {
                        that.corners[i].speed = speedPerTick/10;
                    }
                    //that.corners[i].passed = true;
                    that.corners[i].throttleSpeeds.push(throttleSpeed);
                }
                //that.corners[i].throttleSpeeds.push(throttleSpeed);
                that.corners[i].dataGathered = true;
                
            }
        }
        
        
    };
    
    this.getNextCornerId = function(index) {
        
        for (var i = 0; i < that.corners.length; i++) {
            if (that.corners[i].startIndex > index) {
                return that.corners[i].startIndex;
            }
        }
        
        return that.corners[0].startIndex;
        
    }
    this.getCurrentCornerSpeed = function(index) {
        for(var i = 0; i < that.corners.length; i++) {
            if (index >= that.corners[i].startIndex && index <= that.corners[i].endIndex ) {
                if (that.corners[i].speed > 1) {
                    return 1;
                }
                else {
                    return that.corners[i].speed;
                }
            }
        }
    };

    this.getCurveSpeed = function(car, piece, currentSpeed) {
        
        var index = car.piecePosition.pieceIndex;
        for(var i = 0; i < that.corners.length; i++) {
            var corner = that.corners[i];
            if (corner.startIndex <= index && corner.endIndex >= index) {
                
                if (index === corner.endIndex && corner.carAngles.length > 2) {
                    //console.log(Math.abs(corner.carAngles[corner.carAngles.length-2]) + " " + Math.abs(car.angle) + " = " + Math.abs(Math.abs(corner.carAngles[corner.carAngles.length-2]) - Math.abs(car.angle)))
                    if (Math.abs(Math.abs(corner.carAngles[corner.carAngles.length-2]) - Math.abs(car.angle)) < 0.7 || Math.abs(corner.carAngles[corner.carAngles.length-1]) > Math.abs(car.angle)) {
                        //console.log("FUUUUU!!! " + car.angle + " --- " + corner.carAngles[corner.carAngles.length-1] + " --- " + corner.carAngles[corner.carAngles.length-2])
                        return 1;
                    } 
                }
                
                var angle = corner.angle;
                var radius;
                if (angle < 0) {
                    radius = corner.radius + lanes[car.piecePosition.lane.startLaneIndex].distanceFromCenter;
                }
                else {
                    radius = corner.radius - lanes[car.piecePosition.lane.startLaneIndex].distanceFromCenter;
                }
                
                corner.speed = Math.sqrt((radius) * magicNumber)/10;
                /*
                if (pieces[corner.startIndex-1].length && Math.abs(car.angle) < 30 && corner.pieces.length === 1 && (pieces[index+1].length || ( Math.abs(pieces[index+1].angle) < 25 && pieces[index+2].length))) {
                    corner.speed *= 1.4;
                }
                else if (pieces[corner.startIndex-1].length && Math.abs(car.angle) < 30 && corner.pieces.length === 2 && (pieces[corner.endIndex+1].length || 
                        ( ( Math.abs(pieces[corner.endIndex+1].angle) < 25 && pieces[corner.endIndex+2].length)) )) {
                    corner.speed *= 1.3;
                }
                
                if (corner.speed > 1) {
                    corner.speed = 1;
                }*/
                
                //console.log("TARGET: " + corner.speed + " " + currentSpeed)
                if (corner.speed < currentSpeed) {
                    //console.log("BRAKE")
                    return getExactBrakeSpeed(currentSpeed*10, corner.speed);
                }
                else {
                    var newSpeed = getExactThrottleSpeed(currentSpeed*10, corner.speed);
                    return newSpeed;
                }
                
            }
        }
        return currentSpeed;
    };
    
    this.getCornerSpeed = function (index, carAngle, lap) {
        
        for(var i = 0; i < that.corners.length; i++) {
            if (that.corners[i].startIndex === index) {
                that.corners[i].throttleSpeeds = [];
                var speed;
                //console.log("CORNER " + i + " - CAR ANGLE: " + that.corners[i].getMaxAngle() + ", SPEED: " + that.corners[i].speed);
                if (false && that.corners[i].dataGathered) {
                    var oldSpeed = that.corners[i].speed;
                    if (that.corners[i].passed) {
                        if (that.corners[i].getMaxAngle() < (that.corners[i].crashAngle - 5) && !that.corners[i].keepSpeed) {
                            var loose = that.corners[i].crashAngle - that.corners[i].getMaxAngle();
                            console.log("PREVIOUS ANGLE ON NEXT CORNER: " + that.corners[i].getMaxAngle() + " LOOSE ANGLE: " + loose);
                            that.corners[i].speed = that.corners[i].speed + (loose/that.corners[i].crashAngle)*0.01;
                            console.log("INCREASE THROTTLE TO " + that.corners[i].speed);
                            that.corners[i].prevSpeed = oldSpeed;
                            speed = that.corners[i].speed;

                            if (speed > 1) {
                                speed = 1;
                            }
                            log("OLD SPEED: " + oldSpeed + ", NEW SPEED: " + speed);
                            console.log("OLD SPEED: " + oldSpeed + ", NEW SPEED: " + speed);
                        }
                        else {
                            speed = that.corners[i].speed;
                            that.corners[i].keepSpeed = true;
                            console.log("DONT CHANGE SPEED SINCE LAST TIME")
                        }
                    }
                    else {
                        if (that.corners[i].prevSpeed) {
                            speed = that.corners[i].prevSpeed;
                        }
                        else if (that.corners[i].driftAngleIncreaseAlert) {
                            speed = that.corners[i].speed - 0.1;
                        }
                        else {
                            speed = that.corners[i].speed -0.01;
                        }
                        console.log("NEW SPEED after CRASH: " + speed);
                    }
                }
                else {
                    speed = that.corners[i].speed;
                }
                
                
                return speed;
            }
        }
        
        return 1;
        
    };
    
    
    function corner(startIndex) {
        var self = this;
        this.startIndex = startIndex;
        this.endIndex;
        this.pieces = [];
        this.angleSum = 0;
        this.radiusSum = 0;
        this.radiusAvg = 0;
        this.angle;
        this.radius;
        
        this.carAngles = [];
        this.throttleSpeeds = [];
        
        this.speed = 0;
        this.prevSpeed = 0;
        this.passed = true;
        this.driftAngleIncreaseAlert = false;
        this.dataGathered = false;
        this.keepSpeed = false;
        this.crashAngle = 60;
        
        this.addToAngles = function(angle) {
            self.carAngles.push(angle);
        };

        this.getMaxAngle = function() {
            var maxAngle = 0;
            for (var i = 0; i < self.carAngles.length; i++) {
                if (self.carAngles[i] > maxAngle) {
                    maxAngle = self.carAngles[i];
                }
            }
            return maxAngle;
        }
        
        this.restore = function(temp) {
            self.endIndex = temp.endIndex;
            self.pieces = temp.pieces;
            self.carAngles = temp.carAngles;
            self.throttleSpeeds = temp.throttleSpeeds;
            self.speed = temp.speed;
            self.prevSpeed = temp.prevSpeed;
            self.passed = temp.passed;
            self.dataGathered = true;
            self.keepSpeed = temp.keepSpeed;
            self.angleSum = temp.angleSum;
            self.radiusSum = temp.radiusSum;
            self.radiusAvg = temp.radiusAvg;
            self.crashAngle = temp.crashAngle;
        };
        
        this.init = function() {
            var maxAngle = 0;
            var minRadius = 1000;
            for (var i = 0; i < self.pieces.length; i++) {
                self.angleSum += Math.abs(self.pieces[i].angle);
                self.radiusSum += self.pieces[i].radius;
                if (Math.abs(self.pieces[i].angle) > maxAngle) {
                    maxAngle = Math.abs(self.pieces[i].angle);
                }
                if (Math.abs(self.pieces[i].radius) < minRadius) {
                    minRadius = Math.abs(self.pieces[i].radius);
                }
            }

            //self.radiusAvg = self.radiusSum / self.pieces.length;
            //self.speed = 1 - self.angleSum / self.radiusAvg / 4;
            //console.log("ANALYZED CURVE SPEED " + self.startIndex + ": 1 - " + self.angleSum + " / " + self.radiusAvg + " / 3 = " + self.speed);
            
                self.speed = Math.sqrt((minRadius-10) * magicNumber)/10;
                console.log(self.startIndex + "-" + self.endIndex + ": " + self.speed);

                        
            if (self.pieces.length <= 2 && pieces[self.startIndex-1].length && pieces[self.endIndex+1].length) {
                self.speed *= 1.25;
                if (self.speed > 1) {
                    self.speed = 1;
                }
            }
            
            
        };
    }

    
}